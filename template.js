//TEMPLATE DE LA FACTURA
function getFacturaTemplate(factura, espacio, editable) {
  let rows = "";
  for (let i = 0; i < factura.productos.length; i++) {
    const product = factura.productos[i];
    const row = getProductTemplate(product, i, editable);
    rows = rows + row;
  }
  const template = `
  <div>
    <div>
    <h2>${espacio}</h2>
    <table style="width: 100%">
      <tr>
        <th>Producto</th>
        <th>Cantidad</th>
        <th>Precio Unitario</th>
        <th>Total</th>
      </tr> 
      ${rows}
      <tr>
        <th colspan="3">Total</th> 
        <th>${factura.total.toFixed(2)}</th>
      </tr>
      <tr>
        <th><button id="cerrar" onclick="insertFact(factura)">Cerrar Factura</button></th>
      </tr>
    </table>
    </div>
  </div>
 `;
  return template;
}
//TEMPLATE DE LOS PRODUCTOS
function getProductTemplate(product, position, editable) {
  let imput = `<input type="text" id="cantidad_${position}" value ="${product.cantidad}">`;
  let btnModificar = `<button name="modCantidad" onclick ="editCantidad('${product.id}', ${position})" >Modificar</button>`;
  let btnEliminar = ` <button name="delete" onclick ="deleteProduct('${product.id}', ${position})" >Eliminar</button>`;
  if (editable === false) {
    imput = `${product.cantidad}`;
    btnModificar = "";
    btnEliminar = "";
  }
  const result = `
  <tr>
    <td>${product.nombre}</td>
    <td>
      ${imput}
    </td>
    <td>${product.precio}</td>
    <td style="font-weight: bold">${product.precio * product.cantidad}</td>
    <td>
    ${btnModificar} 
    </td>
    <td>
     ${btnEliminar}
    </td>
    </tr>`;
  return result;
}

//TEMPLATE DEL REPORTE X
function getTotalVentas() {
  let countProcessed = contadordefactura(factura);
  let reportTotal = calculateTotalGlobal(getFacturas());
  const resultList = `
  <tr>
    <td>${countProcessed}</td>
    <td>${reportTotal.toFixed(2)}</td>
  </tr>
  `;
  return resultList;
}

//TEMPLATE DEL REPORTE DETALLADO
function getListFactTemplate() {
  const lista = getTotalVentas();
  const listaActual = getFacturas();
  let rows = "";
  for (let i = 0; i < listaActual.length; i++) {
    const invoice = listaActual[i];
    const row = ListaFacturas(invoice, i);
    rows = rows + row;
  }
  const detailFact = `<div>
  <h1>REPORTE DETALLADO</h1>
  <div>
  <table style="width: 40%">
  <tr>
      <th>N° FACTURA</th>
      <th>Nº PRODUCTOS</th>
      <th>MONTO TOTAL</th>
  </tr>
  <tr>
      ${rows}
  </tr>
      </table>
  </div>
  </div>
  <table style= "width:30%">
  <br>
  <br>
    <div>
    <tr>
      <th>FACTURAS PROCESADAS</th>
       <th>TOTAL PROCESADO</th>
       <hr>
     </tr>
     <tr>
     ${lista}
     </tr>
</div>
</table>`;
  return detailFact;
}

function ListaFacturas(fact) {
  const resultList = `
  <tr>  
    <td>COD: 00${fact.numberFact}</td>
    <td>${fact.productos.length}</td>
    <td>${fact.total.toFixed(2)}</td>
    <td><button onclick = "viewDetail(${fact.numberFact})">Detallado</button></td>
  </tr>
  `;
  return resultList;
}

//LA FUNCION MAS PODEROSA DEL TEMPLATE
function showTemplate(template, destino) {
  destino.innerHTML = template;
}

