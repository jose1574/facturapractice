//function que busca una facturas
function buscarFactura(numberFact) {
  const lista = getFacturas();
  const factEncontrada = lista.find((factura) => factura.numberFact === numberFact);
  return factEncontrada;
}

// function that find the product
function srvFindProduct(codigo) {
  const plist = getProductList();
  const found = plist.find((product) => product.id == codigo);
  // console.log(found);
  return found;
}

function srvSubtotalMap(factura) {
  let arrayPrecio = factura.productos;
  let total = arrayPrecio.reduce(acumularMonto, 0);
  return total;
}

function acumularMonto(acumulado, renglon) {
  let totalRenglon = renglon.precio * renglon.cantidad;
  acumulado = acumulado + totalRenglon;
  return acumulado;
}
