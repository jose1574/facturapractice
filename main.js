//ESTA FUNCION BUSCA EL PRODUCTO
function scanProduct() {
    let codigo = document.getElementById("codigoProducto").value;
    let articulo = srvFindProduct(codigo)
    const producto = {
        id: articulo.id,
        nombre: articulo.nombre,
        precio: articulo.precio,
        cantidad: 1,
    }
    factura.productos.push(producto);
    factura.total = srvSubtotalMap(factura)
    updateView(factura);
}
//ESTA FUNCION ACTUALIZA LA VISTA DE NUESTROS PRODUCTOS
function updateView() {
    let editable = true;
    let espacio = `Espacio: Viveres`;
    let template = getFacturaTemplate(factura, espacio, editable);
    let destino = document.getElementById("resultado");
    showTemplate(template, destino);
}

function viewDetail(numberFact) {
    let facturaToShow = buscarFactura(numberFact);
    let editable = false;
    let espacio = `Espacio: Viveres`;
    let template = getFacturaTemplate(facturaToShow, espacio, editable);
    let destino = document.getElementById("resultado");
    showTemplate(template, destino);
    return console.log(facturaToShow);
}

//ESTA FUNCION ACTUALIZA EL TEMPLATE DE MI LISTA DE FACTURAS
function viewReportDetail() {
    let detailTemplate = getListFactTemplate();
    let destino = document.getElementById("resultado");
    showTemplate(detailTemplate, destino);
}

function deleteProduct(product, posicion) {
    factura.productos.splice(posicion, 1);
    factura.total = srvSubtotalMap(factura)
    // 4 - actualizar la vista de la factura
    updateView(factura);
}

function insertFact() {
    //listFact.facturas.push({ numberFact, productos: JSON.parse(JSON.stringify(productos)), total });
    //01 LLAMAMOS A LA FUNCION SAVEFACTURA DEL STORAGE PARA GUARDAR LA FACTURA EN EL LOCALSTORAGE
    codigoFactura(factura);
    saveFactura(factura);
    limpiarlistProduct(factura);
    if (factura.productos.length == 0) {
        factura.total = 0;
    }
    updateView();
}

//FUNCION QUE ME ASIGNA UN CODIGO A CADA FACTURA (CONTADOR)
function codigoFactura(factura) {
    //ORGANIZAR EL NOMBRE DEL CODIGO DE FACTURA NUMBERFACT = CODIGOFACTURA
    const codigo = factura.numberFact += 1;
    return codigo;
}

//ESTA FUNCION ME CALCULA LA CANTIDAD DE FACTURAS PROCESADAS
function contadordefactura() {
    //PARAMETRIZAR LA FUNCION PARA VOLVERLA REUTILIZABLE
    const listFacturas = getFacturas();
    let facturasSaved = listFacturas.length;
    return facturasSaved;
}
//FUNCION QUE BORRA TODOS LOS ARTICULOS GUARDADOS EN FACTURA.PRODUCTOS
function limpiarlistProduct(factura) {
    const limpiar = factura.productos.splice(0, factura.productos.length)
    return limpiar;
}
//ESTA FUNCION ME CALCULA EL TOTAL DE LAS FACTURAS PROCESADAS
function calculateTotalGlobal() {
    //FALTA PARAMETRIZAR LA FUNCION PARA VOLVERLA REUTILIZABLE
    const listFacturas = getFacturas();
    const totales = listFacturas.reduce((acc, item) => {
        return acc += item.total;
    }, 0);
    return totales;
}
//ESTA FUNCION ME EDITA LA CANTIDAD DE ARTICULOS EN LA FACTURA
function editCantidad(product, position) {
    let elementId = `cantidad_${position}`;
    console.log(elementId)
    let cantidad = document.getElementById(`cantidad_${position}`).value;
    let articulos = factura.productos;
    product = articulos[position];
    product.cantidad = parseInt(cantidad);
    factura.total = srvSubtotalMap(factura);
    updateView();
}

