//STORAGE DE LA LISTA DE PRODUCTOS
function getProductList() {
  const list = [
    { id: "001", nombre: "HARINA", precio: 3.3 },
    { id: "002", nombre: "ARROZ", precio: 2.5 },
    { id: "003", nombre: "PASTA", precio: 8.5 },
    { id: "004", nombre: "CARAOTAS", precio: 1.5 },
    { id: "005", nombre: "AZUCAR", precio: 4.5 },
  ];
  return list;
}

// function saveFactura(newFactura) {
//   if (localStorage.getItem("Facturas") === null) {
//     let facturas = [];
//     facturas.push(param);
//     localStorage.setItem("Facturas", JSON.stringify(facturas));
//   } else {
//     let facturas = JSON.parse(localStorage.getItem("Facturas"));
//     facturas.push(param);
//     localStorage.setItem("Facturas", JSON.stringify(facturas));
//   }
// }

function saveFactura(newFactura) {
  const facturas = getFacturas();
  facturas.push(newFactura);
  localStorage.setItem("Facturas", JSON.stringify(facturas));
  return facturas;
}

function getFacturas() {
  if (localStorage == null) {
    throw new Error("querido usuário usted no tiene localStorage");
  }
  const listaActual = localStorage.getItem("Facturas");
  const facturas = listaActual === null ? [] : JSON.parse(listaActual);
  return facturas;
}


// STORAGE DE LA FACTURA ACTUAL
let factura = {
  numberFact: 0,
  productos: [],
  total: 0,
};
